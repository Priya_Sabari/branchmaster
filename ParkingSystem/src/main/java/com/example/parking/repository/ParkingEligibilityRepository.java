package com.example.parking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.example.parking.entity.ParkingSpotRequest;




@Repository
public interface ParkingEligibilityRepository extends JpaRepository<ParkingSpotRequest, Integer > {

	

}
