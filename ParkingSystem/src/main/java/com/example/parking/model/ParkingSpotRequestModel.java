package com.example.parking.model;

import java.time.LocalDate;

public class ParkingSpotRequestModel {
	private Long empId;
	private LocalDate requestedForDate;
	
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public LocalDate getRequestedForDate() {
		return requestedForDate;
	}
	public void setRequestedForDate(LocalDate requestedForDate) {
		this.requestedForDate = requestedForDate;
	}
	public ParkingSpotRequestModel(Long empId, LocalDate requestedForDate) {
		super();
		this.empId = empId;
		this.requestedForDate = requestedForDate;
	

}
	public ParkingSpotRequestModel() {
		super();
		// TODO Auto-generated constructor stub
	}
}
