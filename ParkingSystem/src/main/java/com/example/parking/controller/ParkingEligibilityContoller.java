package com.example.parking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.parking.entity.ParkingSpotEntity;
import com.example.parking.model.ParkingSpotRequestModel;
import com.example.parking.service.ParkingEligibilityService;

@RestController
@RequestMapping("/parking")
public class ParkingEligibilityContoller {

	@Autowired
	private ParkingEligibilityService service;
	
	
	
	@PostMapping("/requestLot")
	public ResponseEntity<String> requestForParkingLot(ParkingSpotRequestModel spot) {
		return new ResponseEntity<String>(service.parkingSpotRequest(spot), HttpStatus.OK);

}
	
	/*@GetMapping("/check")
	public ParkingSpotEntity checkAvailability(@PathVariable("empid") int id)

	{

		ParkingSpotEntity spot =service.checkSlotAvilable(id);

		return spot;

	}*/
	
	
}