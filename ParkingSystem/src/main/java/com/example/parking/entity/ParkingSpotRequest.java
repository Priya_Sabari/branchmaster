package com.example.parking.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ParkingSpotRequest {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "requested_for_date")
	private LocalDate requestedForDate;

	@ManyToOne()
	@JoinColumn(name = "emp_id")
	private EmployeeEntity emp;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getRequestedForDate() {
		return requestedForDate;
	}

	public void setRequestedForDate(LocalDate requestedForDate) {
		this.requestedForDate = requestedForDate;
	}

	public EmployeeEntity getEmp() {
		return emp;
	}

	public void setEmp(EmployeeEntity emp) {
		this.emp = emp;
	}

	@Override
	public String toString() {
		return "SpotRequest [id=" + id + ", requestedForDate=" + requestedForDate + ", emp=" + emp + "]";
	}

	public ParkingSpotRequest(Integer id, LocalDate requestedForDate, EmployeeEntity emp) {
		super();
		this.id = id;
		this.requestedForDate = requestedForDate;
		this.emp = emp;
	}

	public static Object builder() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	


}
