package com.example.parking.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="parkingSpot")
public class ParkingSpotEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int parkingSpotId;
	private int spotnumber;
	private String block;
	private String wing;
	private String location;
	private int noOfDays;
	private boolean allocated;
	
	
	public int getParkingSpotId() {
		return parkingSpotId;
	}
	public void setParkingSpotId(int parkingSpotId) {
		this.parkingSpotId = parkingSpotId;
	}
	public int getSpotnumber() {
		return spotnumber;
	}
	public void setSpotnumber(int spotnumber) {
		this.spotnumber = spotnumber;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getWing() {
		return wing;
	}
	public void setWing(String wing) {
		this.wing = wing;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}
	public boolean isAllocated() {
		return allocated;
	}
	public void setAllocated(boolean allocated) {
		this.allocated = allocated;
	}
	public void setBuildNumber(String block2) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	
	
	
	/*@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)  
	@JoinColumn(name = "vid")
	private VehicleCategory category;
	*/
	
	
}
