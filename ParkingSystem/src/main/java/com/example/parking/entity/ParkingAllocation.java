package com.example.parking.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="allocation")
public class ParkingAllocation {
	
@Id	
private int aid;
private Long empid ;
private int parkingid;
private  boolean allocated;

public int getAid() {
	return aid;
}
public void setAid(int aid) {
	this.aid = aid;
}

public Long getEmpid() {
	return empid;
}
public void setEmpid(Long empid) {
	this.empid = empid;
}
public int getParkingid() {
	return parkingid;
}
public void setParkingid(int parkingid) {
	this.parkingid = parkingid;
}
public boolean isAllocated() {
	return allocated;
}
public void setAllocated(boolean allocated) {
	this.allocated = allocated;
}


}
