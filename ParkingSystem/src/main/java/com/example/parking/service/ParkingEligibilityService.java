package com.example.parking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.parking.entity.ParkingSpotRequest;
import com.example.parking.model.ParkingSpotRequestModel;
import com.example.parking.repository.EmployeeRepository;
import com.example.parking.repository.ParkingEligibilityRepository;





@Service
public class ParkingEligibilityService {

	@Autowired
	private ParkingEligibilityRepository pRepo;
		
	
	@Autowired
	private EmployeeRepository empRepo;


	public String parkingSpotRequest(ParkingSpotRequestModel spot) {

		pRepo.save(ParkingSpotRequest.builder().requestedForDate(spot.getRequestedForDate())
				.employee(empRepo.findById(spot.getEmpId()).get()).build());
		
		return "Requested for Parkinglot";

}




	
	

}
	
	/*
	@PostMapping("/vehicleAllotment")
	public String vehicleAllotment(@RequestBody VehicleAllotmentDto vadto) {
		
		Long noOfAllotments=repo.count();
		Long Capacity=15000l;// we have to fetch from db
		Long availabilityOfAllotments=Capacity-noOfAllotments;
		
		if(availabilityOfAllotments>0) {
			VehicleAllotment vehicleAllotment=new VehicleAllotment();
			SimpleDateFormat formatter=new SimpleDateFormat("dd/m/yyyy HH:mm:ss");
			Date date=new Date();
			System.out.println(formatter.format(date));
			
			VehicleAllotment.setPhoneno(vadto.getPhoneno());
			VehicleAllotment.setVehicleno(vadto.getVehicleno());
			
			VehicleCategory vehicleCategory=new VehicleCategory();
			VehicleCategory.setVid(VehicleAllotment.getCategory().getVid());
			VehicleCategory.setFloorno(VehicleAllotment.getCategory().getFloorno());
			VehicleCategory.setSlotno(VehicleAllotment.getCategory().getSlotno());
			VehicleCategory.setVtype(VehicleAllotment.getCategory().getVtype());
			
			
			VehicleAllotment.setCategory(vehicleCategory);
			
			repo.save(VehicleAllotment);
			return vadto.getCategory().getVtype()+""+vadto.getVehicleno() + "successfully alloted";
		}
		else
		{
		}
		
			return "no vacancy for" + vadto.getCategory().getVtype();
			
		}
	
	
}
*/
	

	
	

