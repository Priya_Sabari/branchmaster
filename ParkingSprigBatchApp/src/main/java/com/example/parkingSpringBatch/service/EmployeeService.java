package com.example.parkingSpringBatch.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.parkingSpringBatch.entity.Employee;
import com.example.parkingSpringBatch.exception.UserNotFoundException;
import com.example.parkingSpringBatch.repository.EmployeeRepository;


@Service
@Transactional
public class EmployeeService<eRepo>   {
	
	
	@Autowired
	private EmployeeRepository eRepo;
	
	 public  Employee getEmployeeById(Long empId) throws UserNotFoundException 
	    {
	        Optional<Employee> employee = eRepo.findById(empId);
	         
	        if(employee.isPresent()) {
	            return employee.get();
	        } else {
	            throw new UserNotFoundException("No employee record exist for given id");
	        }
	    }
	
}
