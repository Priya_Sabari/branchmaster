package com.example.parkingSpringBatch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.parkingSpringBatch.entity.Employee;



@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
