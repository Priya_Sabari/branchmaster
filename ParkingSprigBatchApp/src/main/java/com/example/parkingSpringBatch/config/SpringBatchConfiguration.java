package com.example.parkingSpringBatch.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.example.parkingSpringBatch.entity.Employee;






@Configuration
@EnableBatchProcessing
public class SpringBatchConfiguration {
	

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	public DataSource dataSource;

	@Autowired
	public EntityManagerFactory entityManagerFactory;



	@Bean
	public FlatFileItemReader<Employee> reader() {
		FlatFileItemReader<Employee> reader = new FlatFileItemReader<Employee>();
		reader.setResource(new ClassPathResource("employes.csv"));
		reader.setLineMapper(new DefaultLineMapper<Employee>() {
			{

			setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] {"emp_id", "emp_name", "designation", "experience"});

					}

				});

				setFieldSetMapper(new BeanWrapperFieldSetMapper<Employee>() {

					{

						setTargetType(Employee.class);

					}

				});

 reader.setLinesToSkip(1);

			}

			
			
		});

		return reader;

	}

	@Bean
	public JdbcBatchItemWriter<Employee> writer() {

		JdbcBatchItemWriter<Employee> writer = new JdbcBatchItemWriter<Employee>();

		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Employee>());

		writer.setSql(

				"INSERT INTO employee(emp_id,emp_name,designation,experience) VALUES (:empId,:empName,:designation,:experience)");

		writer.setDataSource(dataSource);
		return writer;

	}
	

	@Bean
	public Job readUserCSVFile() {

		return jobBuilderFactory.get("Job1").incrementer(new RunIdIncrementer()).flow(step1()).end().build();
	}
	@Bean
	public Step step1() {

		return stepBuilderFactory.get("step1").<Employee, Employee>chunk(1).reader(reader()).writer(writer()).build();

	}	

	

}	
	
