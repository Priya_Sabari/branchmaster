package com.example.parkingSpringBatch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.parkingSpringBatch.entity.Employee;
import com.example.parkingSpringBatch.exception.UserNotFoundException;
import com.example.parkingSpringBatch.service.EmployeeService;






@RestController
@RequestMapping("/employees")
public class EmployeeController 
{
	@Autowired
	public EmployeeService service;
		

	  @GetMapping("/empId}")
	  public Employee getEmployeeById(@PathVariable("empId") Long empId) throws UserNotFoundException   {
	  Employee entity = service.getEmployeeById(empId);
	  return entity;
	  }
	 

}
